<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 2/22/2022
  Time: 9:11 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Calculator</title>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<script src="assets/JS/index.js"></script>

<body>
<div class="container">
<%--        <input class="field" type="text" placeholder="0">--%>
    <div class="field">
        <p id ="history">9*9</p>
        <p id="output">81</p>
    </div>
    <div class="number">
        <button class="btn-opp" value="c">c</button>
        <button class="btn-opp" value="ce">CE</button>
        <button class="btn-opp" value="(">(</button>
        <button class="btn-opp" value=")">)</button><br>



        <button class="btn-num" value="1">1</button>
        <button class="btn-num" value="2">2</button>
        <button class="btn-num" value="3">3</button>
        <br>

        <button class="btn-num" value="4">4</button>
        <button class="btn-num" value="5">5</button>
        <button class="btn-num" value="6">6</button>
        <br>

        <button class="btn-num" value="7">7</button>
        <button class="btn-num" value="8">8</button>
        <button class="btn-num" value="9">9</button>
        <br>

        <button class="btn-opp operator" value="+">+</button>
        <button class="btn-opp operator" value="-">-</button>
        <button class="btn-opp operator" value="*">*</button>
        <button class="btn-opp operator" value="%">%</button>
<br>


        <button class="btn-opp" value="0">0</button>
        <button class="btn-opp" value=".">.</button>
        <button class="btn-opp operator" value="/">/</button>
        <button class="btn-opp" value="=">=</button>




    </div>
</div>
</body>
</html>
